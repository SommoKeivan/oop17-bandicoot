package characters;

public interface Position {
	
	double getX();

	double getY();

}
